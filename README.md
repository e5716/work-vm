# My Work Machine

An automated generation of a virtual machine running Archlinux with my work tools and configurations, built on top of Vagrant, Ansible and my [Archlinux automations](https://gitlab.com/e5716/archlinux)

[[_TOC_]]

# Requirements

- Virtualbox
- Virtualbox Extension Pack
- Vagrant

Important: make sure you have enabled Swap / Windows Pagefile on the host machine

# How to use

1. Clone this project into your `$HOME` so the shared folders are not broken. Yes, even on Winblow$ under `C:\Users\yourself`
1. Under `extras/certificates` put any certificates you need. They are usually required when your laptop has Zero Trust configured.
1. From the directory of the project run `vagrant up` everytime you use the machine. The first run will run the provisioning automatically; don't need to run provisioning afterwards.
1. Power off the machine
1. If you want to destroy the instance, run `vagrant destroy -f`
1. When the machine has been created, take a look to the **Manual Steps**

Be patient, provisioning takes its time. And unfortunately Ansible does not show progress information on the terminal when running the tasks.

Optional: to run again the provisioning later, run `vagrant provision`. But this is usually not needed.

I also suggest to run `vagrant box update` from time to time so the image keeps refreshed. And running `yay -Syu` to keep the system updated.

## Manual steps

1. Configure the keyboard layout
1. For Firefox, on its first run, execute `firefox -P`, delete the default profile and mark the Work profile as default. Otherwise the `user.js` settings won't be applied to your running instance, because you'll be running another Firefox profile.
1. You should configure either Vagrantfile or using symlinks for integrating cloud-synced folders into the shared folders, so they can be accesible by your VM
1. Disable the auto-lock screen under your desktop environment, i.e., your newly created VM or sometimes you won't be able to unlock it because this user does not have a password
1. Extra: create a desktop shortcut on the host so you can run this machine quickier:
- Target: `$HOME\.work-machine\run-work-machine.cmd`
- Start in (cwd): `$HOME\.work-machine`
1. Extra: remove and replace Archlinux `$HOME` folders with the host folders, like:
```
ln -s /mnt/prietosoft-winblows/Downloads Downloads
ln -s /mnt/prietosoft-winblows/Documents Documents
ln -s /mnt/prietosoft-winblows/Pictures Pictures
ln -s /mnt/prietosoft-winblows/OneDrive Public
```

# Reasons

- I need portability each time my current company gives me a new laptop or I prefer to start a new VM from scratch.
- I don't want to work with Windows
- It's just too much time invested in configuring ArchLinux for my work needs
- Having a virtual machine, you don't suffer with unsupported common hardware drivers like Wifi cards, as they are handled by your host machine

# Notes

## Users

The default user login is `vagrant:vagrant` and it comes from the Vagrant box where this project is based.

The configured user login is `work:` (empty password)

## For Windows

This project was created in mind to be ran from a Windows host, but it should run fine on a Linux host too.

Please DON'T use WSL, install the requirements directly on Windows to avoid mixing environments. For example, if you install Vagrant on WSL it won't be able to detect your Virtualbox installation.

In fact, I suggest you to disable ALL the Windows features for virtualization and WSL. On my experience, working with Vagrant and Ansible in Windows is a russian roulette.

The project uses `ansible_local` from Vagrant, because I wasn't able to install Ansible on Windows easily. I prefer simplicity.

Make sure to disable Hyper-V on Windows, [according to Vagrant docs](https://www.vagrantup.com/docs/installation#windows-virtualbox-and-hyper-v)

For some reason, Virtualbox v6.1.28 does not work on my Windows work laptop when running a virtual machine, it crashes with the error "Failed to load VMMR0.r0". I'm not sure whether an IT-controlled configuration prevents my virtual machine from running (probably the Windows Defender which I cannot disable).

- If you suffer the same problem, the version 6.1.26 r145957 works fine for me

When the provisioning ends, reboot your new virtual machine. I decided not to include the `reboot` module in Ansible playbook to avoid the `Running reboot with local connection would reboot the control node` error: remember the master machine is also running on the virtual machine. Thank you Winblow$!

Sometimes the first `vagrant up` (which triggers the provisioning) crashes. I noticed this because I have the Logitech Options installed on my machine and it notifies me when a key changes its status. I notice the VM crashing when I open the VM window and the notification of "Scroll lock key <enabled/disabled>" appears on an infinite loop. I don't know the reason behind this, probably is a bug. I just power off the VM and destroy the machine with `vagrant destroy -f`, then start over.

I had to separate the two playbooks found on this repo and running provisioning twice on the Vagrantfile. This happens as a consequence of Ansible not recognizing the AUR collection if I install it earlier on the same playbook, so the solution was to keep separated the installation of the collection and then run the main playbook to configure the VM.

## Development

`auruser` is needed only for the provisioning, it doesn't have a password and it is removed once the provisioning ends. It is configured for running privileged pacman for installing `yay` and the defined packages.

# Future work

- [ ] Be able to build Dataloader successfully within Ansible playbook. I should try to build it using `xvfb-run` just like when creating the Firefox profile, because Dataloader is built successfully when I'm running it inside a graphical session.
- [ ] Virtualization: migrate to VMWare or QEMU (it looks like [it has a Windows version](https://www.qemu.org/download/#windows))
- [ ] Vagrant: encrypt the disk (using Virtualbox encryption features), something like https://gist.github.com/gabrielelana/9189eab5df963deba30f
- [ ] Sway (Wayland) is not working under Virtualbox, even using the `VMSVGA` driver with 3D acceleration enabled and enough video memory and [KMS enabled for that driver](https://wiki.archlinux.org/title/Kernel_mode_setting#Early_KMS_start)
- [ ] Enable thumbnail previews
- [ ] Enable sound with Pipewire
- [ ] Migrate to QEMU/KVM/Virt-Manager so it can be used on Linux hosts

## Known limitations and bugs

- [ ] (Vagrant): customize disks, like defining a partition size, treating them as SSD from VBox configurations. [Currently experimental](https://www.vagrantup.com/docs/disks).
- [ ] (Vagrant, Ansible): Solve the SSH timeouts _(connection reset, connection aborted)_ when running `vagrant up`
- [ ] (Vagrant, Virtualbox): Solve the random freezes when the VM is running. Not sure if that happens because the host runs out of hardware capacity.
- [ ] Fix the build errors to include Dataloader
- [ ] (Ansible) Enable ssh-agent for the work user with systemd; [currently this is not possible](https://stackoverflow.com/questions/59860849/is-there-a-way-to-reliably-check-that-systemd-supports-user) and needs further effort.
- [ ] (Vagrant) sometimes the Windows terminal (i.e. cmd or Powershell) don't display text unless you press Enter and therefore you cannot be aware when Vagrant is displaying new information

# License

MIT