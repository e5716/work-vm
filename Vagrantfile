Vagrant.configure("2") do |config|
  config.vm.box = "archlinux/archlinux"
  # Defining manually the config.vm.base_mac is not needed, according to https://github.com/hashicorp/vagrant/issues/10946

  # Trying to avoid errors of the first "vagrant up" execution because SSH failing
  config.ssh.insert_key = false

  # Provider config
  config.vm.provider "virtualbox" do |vb|
      vb.gui = true
      vb.memory = "6144"
      vb.name = "My Work Machine"

      # Parameters handled by VBoxManage: https://www.virtualbox.org/manual/ch08.html#vboxmanage-modifyvm
      vb.customize ["modifyvm", :id, "--clipboard-mode", "bidirectional"]
      vb.customize ["modifyvm", :id, "--draganddrop", "bidirectional"]
      vb.customize ["modifyvm", :id, "--description", "BTW, I use Arch :)"]
      vb.customize ["modifyvm", :id, "--cpus", "3"]
      vb.customize ["modifyvm", :id, "--vram", "128"]
      vb.customize ["modifyvm", :id, "--hwvirtex", "on"]
      vb.customize ["modifyvm", :id, "--accelerate3d", "on"]
      vb.customize ["modifyvm", :id, "--graphicscontroller", "vmsvga"]
      vb.customize ["modifyvm", :id, "--monitorcount", "2"]
      vb.customize ["modifyvm", :id, "--mouse", "usb"]
      vb.customize ["modifyvm", :id, "--keyboard", "usb"]
      vb.customize ["modifyvm", :id, "--audio", "dsound"] # I found this value running "C:\Program Files\Oracle\VirtualBox\VBoxManage.exe modifyvm" and looking for the row "--audio" to see the available options. It refers to Direct Sounds, as currently I'm running Windows as a host; if running Linux as a host, it should accept "ALSA, PulseAudio, PipeWire, etc"
      vb.customize ["modifyvm", :id, "--audiocontroller", "ac97"]
      vb.customize ["modifyvm", :id, "--audioin", "on"]
      vb.customize ["modifyvm", :id, "--audioout", "on"]
      #vb.customize ["modifyvm", :id, "--usbxhci", "on"] # Enables USB 3.0 and lower versions (EHCI and OHCI). It needs the Oracle VM VirtualBox Extension Pack installed on the host (don't get confused with the VBox's Guest Additions that are optionally installed on each host)
      vb.customize ["modifyvm", :id, "--cableconnected1", "on"]

      # Disabling advanced things I don't use on a daily basis
      vb.customize ["modifyvm", :id, "--recording", "off"]
      vb.customize ["modifyvm", :id, "--vrde", "off"] # VBox's remote machine
      vb.customize ["modifyvm", :id, "--teleporter", "off"]
      vb.customize ["modifyvm", :id, "--tracing-enabled", "off"]
      vb.customize ["modifyvm", :id, "--usbcardreader", "off"]
      vb.customize ["modifyvm", :id, "--autostart-enabled", "off"]
  end

  # Auxiliar synced folders
  config.vm.synced_folder ".", "/vagrant" # Needed for ansible_local provisioner (default shared directory)
  # Synced folders. When provisioning never mount them at /home/work because the whole directory becomes root-owned
  config.vm.synced_folder "..", "/mnt/prietosoft-winblows"

  # Can't run Ansible without this (thanks to zero-trust policies some companies apply), so no Internet connection is made
  config.vm.provision "shell", path: "extras/install-certificates.sh"

  # Ansible will run only the first time the machine is created
  config.vm.provision "ansible_local" do |ansible|
    ansible.install = true
    ansible.install_mode = "default"
    ansible.playbook = "playbooks/prerequisites.yml"
  end
  config.vm.provision "ansible_local" do |ansible|
    #ansible.verbose = "vvv"
    ansible.playbook = "playbooks/configure-work-machine.yml"
  end
end
